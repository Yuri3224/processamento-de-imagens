package processamentoDeImagens;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class abrirImagem {
	public static void main(String[] args)throws IOException {
		 Scanner teclado = new Scanner (System.in);
		
		
		 File arquivo = new File("C:\\Users\\yuriz\\OneDrive\\Imagens\\test.jpg");
		 BufferedImage image = ImageIO.read(arquivo);
		 int altura = image.getHeight();
		 int largura = image.getWidth();
	

		 System.out.println("Qual banda de cor deseja exibir?\n"+ "1)Vermelho\n"+ "2)Verde \n3)Azul"); 
		 int banda = teclado.nextInt();
		 System.out.println("Digite o valor de aumento da banda.");
		 int aumento = teclado.nextInt();
		 
		 BufferedImage imagemsaidaR = bandaR(image, aumento);
		 BufferedImage imagemsaidaB = bandaB(image, aumento);
		 BufferedImage imagemsaidaG = bandaG(image, aumento);
	
		 

		 switch(banda) {
			 case 1:
				 //imagem vermelha
				 
				 JLabel mylabel1 = new JLabel(new ImageIcon(imagemsaidaR));
				 JPanel mypanel1 = new JPanel();
				 mypanel1.add(mylabel1);
				 JFrame myframe1 = new JFrame();
				 myframe1.setSize(largura, altura);
				 myframe1.add(mypanel1);
				 myframe1.setVisible(true);
				 myframe1.setLocationRelativeTo(null);
				 break;
				 
			 case 2:
				 //imagem verde
				 JLabel mylabel2 = new JLabel(new ImageIcon(imagemsaidaG));
				 JPanel mypanel2 = new JPanel();
				 mypanel2.add(mylabel2);
				 JFrame myframe2 = new JFrame();
				 myframe2.setSize(largura, altura);
				 myframe2.add(mypanel2);
				 myframe2.setVisible(true);
				 myframe2.setLocationRelativeTo(null);
				 break;
			 case 3:
				 //imagem azul
				 JLabel mylabel3 = new JLabel(new ImageIcon(imagemsaidaB));
				 JPanel mypanel3 = new JPanel();
				 mypanel3.add(mylabel3);
				 JFrame myframe3 = new JFrame();
				 myframe3.setSize(largura, altura);
				 myframe3.add(mypanel3);
				 myframe3.setVisible(true);
				 myframe3.setLocationRelativeTo(null);

				 break;
				 
			default:
				System.out.println("Op��o inexistente.");
				break;
		 
		 }
		 
		 teclado.close();
		
		}

		
	
	//selecionando s� a banda vermelha
		public static BufferedImage bandaR (BufferedImage image, int aumento) {
			 int altura = image.getHeight();
			 int largura = image.getWidth();
			 BufferedImage imagemsaidaR = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
			 for (int linha = 0; linha < altura; linha++) {
				 	for (int coluna = 0; coluna < largura; coluna++) {
				 		int rgb = image.getRGB(coluna, linha);
				 		Color cor = new Color(rgb);
				 		int red = cor.getRed();
				 		int tonalidade = aumentarTom(aumento, red);
				 		Color vermelhoApenas = new Color(tonalidade, 0, 0);
				 		imagemsaidaR.setRGB(coluna, linha, vermelhoApenas.getRGB());
					 }
			}
			 
			 return imagemsaidaR;
		}
		
		//selecionando s� a banda verde
		public static BufferedImage bandaG (BufferedImage image, int aumento) {
			 int altura = image.getHeight();
			 int largura = image.getWidth();
			 BufferedImage imagemsaidaG = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
			 
			 for (int linha = 0; linha < altura; linha++) {
				 	for (int coluna = 0; coluna < largura; coluna++) {
				 		int rgb = image.getRGB(coluna, linha);
				 		Color cor = new Color(rgb);
				 		int green = cor.getGreen();
				 		int tonalidade = aumentarTom(aumento, green);
				 		Color verdeApenas = new Color(0, tonalidade, 0);
				 		imagemsaidaG.setRGB(coluna, linha, verdeApenas.getRGB());
					 }
			}
			 
			 return imagemsaidaG;
		}
		
		//selecionando s� a banda azul
		public static BufferedImage bandaB (BufferedImage image, int aumento) {
			 int altura = image.getHeight();
			 int largura = image.getWidth();
			 BufferedImage imagemsaidaB = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
			 
			 for (int linha = 0; linha < altura; linha++) {
				 	for (int coluna = 0; coluna < largura; coluna++) {
				 		int rgb = image.getRGB(coluna, linha);
				 		Color cor = new Color(rgb);
				 		int blue = cor.getBlue();
				 		int tonalidade = aumentarTom(aumento, blue);
				 		Color azulApenas = new Color(0, 0, tonalidade);
				 		imagemsaidaB.setRGB(coluna, linha, azulApenas.getRGB());
					 }
			}
			 
			 return imagemsaidaB;
		}
		

		//impedir que ultrapasse 255
		public static int aumentarTom(int pixel, int aumento) {
			
			if(aumento + pixel <= 0) {
				aumento = 0;
				
			}
			else if(aumento + pixel >= 255) {
				aumento = 255;
			}
			else {
				aumento = pixel + aumento;
			}
			
			
			return aumento;
		}

}
