package kernel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;


public class mediaMediana {

	public static void main(String[] args) throws IOException {

		File arquivo = new File("C:\\Users\\yuriz\\Imagens\\lena.png");
		BufferedImage image = ImageIO.read(arquivo);
		int largura = image.getWidth();
		int altura = image.getHeight();
		
		JTabbedPane tab = new JTabbedPane();
		
	
		
		BufferedImage grayScale = bandaGray(image);
		BufferedImage filtroMediana = saidaMediana(image);
		BufferedImage filtroMedia = saidaMedia(image);
		JFrame frame = configurarFrame(altura, largura, image);
		 
		
		
		
		JLabel labelOriginal = new JLabel(new ImageIcon(grayScale));
		JPanel panelOriginal = new JPanel();
		panelOriginal.add(labelOriginal);
		panelOriginal.setVisible(true);
		
		JLabel labelMedia = new JLabel(new ImageIcon(filtroMedia));
		JPanel panelMedia = new JPanel();
		panelMedia.add(labelMedia);
		
		JLabel labelMediana = new JLabel(new ImageIcon(filtroMediana));
		JPanel panelMediana = new JPanel();
		panelMediana.add(labelMediana);
		
		frame.add(BorderLayout.CENTER,tab);
		tab.add("foto original", panelOriginal);
		tab.add("filtro m�dia", panelMedia);
		tab.add("filtro mediana", panelMediana);
		
		
	}
	
	public static JFrame configurarFrame(int altura, int largura, BufferedImage image) {
		JFrame myframe = new JFrame();
		
		myframe.setIconImage(image);
		myframe.setTitle("Filtro M�dia e Mediana ");
		myframe.setSize(largura+120, altura+120);
		myframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		myframe.setLocationRelativeTo(null);
		myframe.setResizable(false);
		myframe.setVisible(true);

		return myframe;
		
	}
	
	public static BufferedImage saidaMediana (BufferedImage image) {
		int altura = image.getHeight();
		int largura = image.getWidth();
		int p1, p2, p3, p4, p5, p6, p7,p8, p9;
		BufferedImage saida = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
		for(int linha = 1; linha < altura - 1; linha++) {
			for(int coluna = 1; coluna < largura - 1; coluna++) {
				
				Color cor1 = new Color(image.getRGB(linha-1 , coluna - 1));
				Color cor2 = new Color(image.getRGB(linha-1 , coluna));
				Color cor3 = new Color(image.getRGB(linha-1 , coluna + 1));
				Color cor4 = new Color(image.getRGB(linha , coluna - 1));
				Color cor5 = new Color(image.getRGB(linha , coluna  ));
				Color cor6 = new Color(image.getRGB(linha , coluna + 1));
				Color cor7 = new Color(image.getRGB(linha+1 , coluna - 1));
				Color cor8 = new Color(image.getRGB(linha+1 , coluna ));
				Color cor9 = new Color(image.getRGB(linha+1 , coluna + 1));
				
				p1 = cor1.getRed();
				p2 = cor2.getRed();
				p3 = cor3.getRed();
				p4 = cor4.getRed();
				p5 = cor5.getRed();
				p6 = cor6.getRed();
				p7 = cor7.getRed();
				p8 = cor8.getRed();
				p9 = cor9.getRed();
				
				int [] mediana = {p1,p2,p3,p4,p5,p6,p7,p8,p9};
				Arrays.sort(mediana);
				Color valorMediado = new Color(mediana[4],mediana[4],mediana[4]);
				saida.setRGB(linha,coluna,valorMediado.getRGB());
				
				
				
				
			}
		}
		
		return saida;
	}

	
	public static BufferedImage saidaMedia (BufferedImage image) {
		int altura = image.getHeight();
		int largura = image.getWidth();
		int p1, p2, p3, p4, p5, p6, p7,p8, p9;
		BufferedImage saida = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
		for(int linha = 1; linha < altura-1; linha++) {
			for(int coluna = 1; coluna < largura-1; coluna++) {
				
				Color cor1 = new Color(image.getRGB(linha-1 , coluna - 1));
				Color cor2 = new Color(image.getRGB(linha-1 , coluna));
				Color cor3 = new Color(image.getRGB(linha-1 , coluna + 1));
				Color cor4 = new Color(image.getRGB(linha , coluna - 1));
				Color cor5 = new Color(image.getRGB(linha , coluna  ));
				Color cor6 = new Color(image.getRGB(linha , coluna + 1));
				Color cor7 = new Color(image.getRGB(linha+1 , coluna - 1));
				Color cor8 = new Color(image.getRGB(linha+1 , coluna ));
				Color cor9 = new Color(image.getRGB(linha+1 , coluna + 1));
				
				p1 = cor1.getRed();
				p2 = cor2.getRed();
				p3 = cor3.getRed();
				p4 = cor4.getRed();
				p5 = cor5.getRed();
				p6 = cor6.getRed();
				p7 = cor7.getRed();
				p8 = cor8.getRed();
				p9 = cor9.getRed();

				int media = (p1 + p2 + p3 + p4 + p5+p6+p7+p8+p9)/9;
			
				Color valorMediado = new Color(media,media,media);
				saida.setRGB(linha,coluna,valorMediado.getRGB());
			}
		}
		return saida;
	}
	
	
		public static BufferedImage bandaGray (BufferedImage image) {
			int altura = image.getHeight();
			int largura = image.getWidth();
			BufferedImage saidaGray = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
			
			for(int linha = 0; linha < altura; linha++) {
				for(int coluna = 0; coluna < largura; coluna++) {
					int rgb = image.getRGB(coluna,linha);
					Color c = new Color(rgb);
					int red = c.getRed();
					int green = c.getGreen();
					int blue = c.getBlue();
					int gray = (red + green + blue) / 3;
					Color bandaunicaB = new Color(gray,gray,gray);
					saidaGray.setRGB(coluna,linha,bandaunicaB.getRGB());
					
				}
			}
			return saidaGray;
	}

}
