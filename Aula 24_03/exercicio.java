package processamentoDeImagens;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;


public class exercicio {
	public static void main(String[] args) throws IOException {
		File imagem = new File ("C:\\Users\\aluno\\Pictures\\demon.jpg");
		BufferedImage img = ImageIO.read(imagem);
		 int altura = img.getHeight();
		 int largura = img.getWidth();

		//CRIANDO OS OBJETOS COLOR
		 Color azul = new Color (0,0,255);
		 Color verde = new Color (0,255,0);
		 Color vermelho = new Color (255,0,0);
        
        //QUEST�O 1: PINTAR O PRIMEIRO PIXEL DA IMAGEM DE AZUL
			img.setRGB(0, 0, azul.getRGB());
        
        //QUEST�O 2: PINTAR O PIXEL CENTRAL DE VERDE
			img.setRGB(largura/2, altura/2, verde.getRGB());
        
        //QUEST�O 3: PINTAR O ULTIMO PIXEL DE VERMELHO
			img.setRGB(largura-1, altura-1, vermelho.getRGB());

		ImageIO.write(img, "png", new File ("C:\\Users\\aluno\\Pictures\\demon2.png"));


		//QUEST�O 4: PERCORRER A IMAGEM PIXEL A PIXEL (USANDO FOR)
		for (int linha = 0; linha < altura; linha++) {
			  for (int coluna = 0; coluna < largura; coluna++) {
                  //QUEST�O 5: EXIBIR RGB DE CADA PIXEL
				  Color c = new Color(img.getRGB(coluna, linha));
				  int r = c.getRed();
				  int g = c.getGreen();
				  int b = c.getBlue();
				  System.out.println("COLUNA: " + coluna + "]" + "[LINHA: " + linha + "]" + "vermelho: "+ r + " verde: "+ g + " azul: "+ b );

			  }
			}
		}

}
