package convolucao;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;


public class metodosConvolucao {

	public static void main(String[] args) throws IOException {

		File arquivo = new File("C:\\Users\\yuriz\\Imagens\\test.jpg");
		BufferedImage image = ImageIO.read(arquivo);
		int largura = image.getWidth();
		int altura = image.getHeight();
		
		//criando os kernels
		int[] horin3x3 = {-1,-2,-1
				      	  ,0,0,0,
				      	   1,2,1};
		
		int[] vert3x3 = {-1,0,1,
						-2,0,2,
						-1,0,1};
		
		int[] west3x3 = {-1,1,-1,
						 1,-2,-1,
						 1,1,-1};

		int[] laplace3x3 = {0,-1,0,
						   -1,4,-1,
						    0,-1,0};
		
		double[] gauss3x3 = {0.0625, 0.125, 0.0625,
							 0.125, 0.25, 0.125,
							 0.0625, 0.125, 0.0625};
		
		//chamando os m�todos
		BufferedImage grayScale = bandaGray(image);
		BufferedImage filtroGauss = saidaGauss(grayScale, gauss3x3);
		BufferedImage filtroHorizon = saidaHorizon(grayScale, horin3x3);
		BufferedImage filtroVert = saidaVert(grayScale, vert3x3);
		BufferedImage filtroWest = saidaWest(grayScale, west3x3);
		BufferedImage filtroLaplace = saidaLaplace(grayScale, laplace3x3);
		
		JTabbedPane tab = new JTabbedPane();
		JFrame frame = configurarFrame(altura, largura, image);
		 
		
		
		//JPanel's
		JLabel labelOriginal = new JLabel(new ImageIcon(grayScale));
		JPanel panelOriginal = new JPanel();
		panelOriginal.add(labelOriginal);
		
		JLabel labelGaussian = new JLabel(new ImageIcon(filtroGauss));
		JPanel panelGaussian = new JPanel();
		panelGaussian.add(labelGaussian);
		
		JLabel labelHorinz = new JLabel(new ImageIcon(filtroHorizon));
		JPanel panelHorinz = new JPanel();
		panelHorinz.add(labelHorinz);
		
		JLabel labelVert = new JLabel(new ImageIcon(filtroVert));
		JPanel panelVert = new JPanel();
		panelVert.add(labelVert);
		
		JLabel labelWest = new JLabel(new ImageIcon(filtroWest));
		JPanel panelWest = new JPanel();
		panelWest.add(labelWest);
		
		JLabel labelLaplace = new JLabel(new ImageIcon(filtroLaplace));
		JPanel panelLaplace = new JPanel();
		panelLaplace.add(labelLaplace);
		
		
		//adicionar os panels em abas
		frame.add(BorderLayout.CENTER,tab);
		tab.add("foto original", panelOriginal);
		tab.add("filtro horinzontal", panelHorinz);
		tab.add("filtro vertical", panelVert);
		tab.add("filtro west", panelWest);
		tab.add("filtro Laplace", panelLaplace);
		tab.add("fitro gaussiano", panelGaussian);

	}
	//Dimens�es do JFrame
	public static JFrame configurarFrame(int altura, int largura, BufferedImage image) {
		JFrame myframe = new JFrame();
		myframe.setTitle("Fotochope");
		myframe.setSize(largura+150, altura+150);
		myframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		myframe.setLocationRelativeTo(null);
		myframe.setResizable(true);
		myframe.setVisible(true);

		return myframe;
		
	}
	
	//M�todo GAUSSIANO
	public static BufferedImage saidaGauss (BufferedImage image, double[] kernel) {
		int altura = image.getHeight();
		int largura = image.getWidth();
		int tamVizinhanca = (int) Math.sqrt(kernel.length);
		int ref = tamVizinhanca/2;
		BufferedImage saidaGauss = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
		for(int linha = ref; linha < altura - ref; linha++) {
			for(int coluna = ref; coluna < largura - ref; coluna++) {
				
				double soma = 0;
				int contador = 0;
			
				for(int linha1 = -ref; linha1 <= ref; linha1++) {
					for(int coluna1 = -ref; coluna1 <= ref; coluna1++) {
						Color cores = new Color(image.getRGB(coluna + linha1, linha + coluna1));
						int vermelho = cores.getRed();
						soma +=(double)vermelho*kernel[contador];
						contador++;
					}
					
				}
				
				Color valorGauss = new Color((int)soma,(int)soma,(int)soma);
				saidaGauss.setRGB(coluna,linha,valorGauss.getRGB());
			}
		}
		
		return saidaGauss;
	}
	
	//M�todo HORIZONTAL
	public static BufferedImage saidaHorizon (BufferedImage image, int[] kernelHorizontal) {
		int altura = image.getHeight();
		int largura = image.getWidth();
		int tamVizinhanca = (int) Math.sqrt(kernelHorizontal.length);
		int ref = tamVizinhanca/2;
		BufferedImage saidaHorizon = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
		for(int linha = ref; linha < altura - ref; linha++) {
			for(int coluna = ref; coluna < largura - ref; coluna++) {
				
				double soma = 0;
				int contador = 0;
			
				for(int linha1 = -ref; linha1 <= ref; linha1++) {
					for(int coluna1 = -ref; coluna1 <= ref; coluna1++) {
						Color cores = new Color(image.getRGB(coluna + linha1, linha + coluna1));
						int vermelho = cores.getRed();
						soma +=(int)vermelho*kernelHorizontal[contador];
						contador++;
					}
					
				}
				
				if(soma > 255) {
					soma = 255;}
				if(soma < 0) {
					soma = 0;}
				
				Color valorHorinzon = new Color((int)soma,(int)soma,(int)soma);
				saidaHorizon.setRGB(coluna,linha,valorHorinzon.getRGB());
			}
		}
		
		return saidaHorizon;
	}
	
	//M�todo VERTICAL
	public static BufferedImage saidaVert (BufferedImage image, int[] kernelVert) {
		int altura = image.getHeight();
		int largura = image.getWidth();
		int tamVizinhanca = (int) Math.sqrt(kernelVert.length);
		int ref = tamVizinhanca/2;
		BufferedImage saidaVert = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
		for(int linha = ref; linha < altura - ref; linha++) {
			for(int coluna = ref; coluna < largura - ref; coluna++) {
				
				double soma = 0;
				int contador = 0;
			
				for(int linha1 = -ref; linha1 <= ref; linha1++) {
					for(int coluna1 = -ref; coluna1 <= ref; coluna1++) {
						Color cores = new Color(image.getRGB(coluna + linha1, linha + coluna1));
						int vermelho = cores.getRed();
						soma +=(int)vermelho*kernelVert[contador];
						contador++;
					}
					
				}
				
				if(soma > 255) {
					soma = 255;}
				if(soma < 0) {
					soma = 0;}
				Color valorHorinzon = new Color((int)soma,(int)soma,(int)soma);
				saidaVert.setRGB(coluna,linha,valorHorinzon.getRGB());
			}
		}
		
		return saidaVert;
	}
	
	//M�todo LINHAS OESTE
	public static BufferedImage saidaWest (BufferedImage image, int[] kernelWest) {
		int altura = image.getHeight();
		int largura = image.getWidth();
		int tamVizinhanca = (int) Math.sqrt(kernelWest.length);
		int ref = tamVizinhanca/2;
		BufferedImage saidaWest = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
		for(int linha = ref; linha < altura - ref; linha++) {
			for(int coluna = ref; coluna < largura - ref; coluna++) {
				
				double soma = 0;
				int contador = 0;
				
				for(int linha1 = -ref; linha1 <= ref; linha1++) {
					for(int coluna1 = -ref; coluna1 <= ref; coluna1++) {
						Color cores = new Color(image.getRGB(coluna + linha1, linha + coluna1));
						int vermelho = cores.getRed();
						soma +=(int)vermelho*kernelWest[contador];
						contador++;
					}
					
				}
				
				if(soma > 255) {
					soma = 255;}
				if(soma < 0) {
					soma = 0;}
				Color valorHorinzon = new Color((int)soma,(int)soma,(int)soma);
				saidaWest.setRGB(coluna,linha,valorHorinzon.getRGB());
			}
		}
		
		return saidaWest;
	}
	
	//M�todoo LAPLACE
	public static BufferedImage saidaLaplace (BufferedImage image, int[] kernelLaplace) {
		int altura = image.getHeight();
		int largura = image.getWidth();
		int tamVizinhanca = (int) Math.sqrt(kernelLaplace.length);
		int ref = tamVizinhanca/2;
		BufferedImage saidaLaplace = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
		for(int linha = ref; linha < altura - ref; linha++) {
			for(int coluna = ref; coluna < largura - ref; coluna++) {
				
				double soma = 0;
				int contador = 0;
				
				for(int linha1 = -ref; linha1 <= ref; linha1++) {
					for(int coluna1 = -ref; coluna1 <= ref; coluna1++) {
						Color cores = new Color(image.getRGB(coluna + linha1, linha + coluna1));
						int vermelho = cores.getRed();
						soma +=(int)vermelho*kernelLaplace[contador];
						contador++;
					}
					
				}
				
				if(soma > 255) {
					soma = 255;}
				if(soma < 0) {
					soma = 0;}
				Color valorlaplace = new Color((int)soma,(int)soma,(int)soma);
				saidaLaplace.setRGB(coluna,linha,valorlaplace.getRGB());
			}
		}
		
		return saidaLaplace;
	}
	
	//Metodo IMAGEM ORIGINAL > GRAYSCALE
	public static BufferedImage bandaGray (BufferedImage image) {
		int altura = image.getHeight();
		int largura = image.getWidth();
		BufferedImage saidaGray = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
		
		for(int linha = 0; linha < altura; linha++) {
			for(int coluna = 0; coluna < largura; coluna++) {
				int rgb = image.getRGB(coluna,linha);
				Color c = new Color(rgb);
				int red = c.getRed();
				int green = c.getGreen();
				int blue = c.getBlue();
				int gray = (red + green + blue) / 3;
				Color bandaunicaB = new Color(gray,gray,gray);
				saidaGray.setRGB(coluna,linha,bandaunicaB.getRGB());
				
			}
		}
		return saidaGray;
}
	

}