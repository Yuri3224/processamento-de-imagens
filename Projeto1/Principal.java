import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.BorderLayout;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;


public class Principal extends JFrame {
	
private static BufferedImage BufferedImage;


public static void main(String[] args)throws IOException {


 File arquivo = new File("C:\\Users\\marck\\Pictures\\test.jpg");
 
 

 BufferedImage image = ImageIO.read(arquivo);
 
 int limiar = 100;
BufferedImage imagemsaidaBin = imagemBinarizada(image, limiar);
 BufferedImage imagemsaidaR = bandaR(image);
 BufferedImage imagemsaidaB = bandaB(image);
 BufferedImage imagemsaidaG = bandaG(image);

 panelAbas(image, imagemsaidaR, imagemsaidaG, imagemsaidaB, imagemsaidaBin, limiar);

}


//----- Frames -----

//jframe normal


public static void panelAbas (BufferedImage image, BufferedImage imagemsaidaR, 
		BufferedImage imagemsaidaG, BufferedImage imagemsaidaB,
		BufferedImage imagemsaidaBin, int limiar) {
		JFrame frame = configurarFrame();
		JButton jb = new JButton("Sair");

		 jb.setBounds(530, 550, 70, 40);

		 //foto normal
		 JLabel mylabel = new JLabel(new ImageIcon(image));
		 JPanel mypanel = new JPanel();
		 mypanel.add(mylabel);
		 mypanel.add(jb);
		 mypanel.setVisible(true);
		 jb.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					System.exit(0);
				}


			});
		 //foto red
		 JLabel mylabelRed = new JLabel(new ImageIcon(imagemsaidaR));
		 JPanel mypanelRed = new JPanel();
		 mypanelRed.add(mylabelRed);
		 //foto green
		 JLabel mylabelGreen = new JLabel(new ImageIcon(imagemsaidaG));
		 JPanel mypanelGreen = new JPanel();
		 mypanelGreen.add(mylabelGreen);
		 //foto blue
		 JLabel mylabelBlue = new JLabel(new ImageIcon(imagemsaidaB));
		 JPanel mypanelBlue = new JPanel();
		 mypanelBlue.add(mylabelBlue);
		 //foto binarizada
		 JLabel labelbin = new JLabel(new ImageIcon(imagemsaidaBin));
		 JPanel panelbin = new JPanel();

		 panelbin.add(labelbin);

		 JTabbedPane tab = new JTabbedPane();
		frame.add(BorderLayout.CENTER,tab);
			tab.add("Foto Normal", mypanel);
			tab.add("Foto Binarizada", panelbin);

		}





public static JFrame configurarFrame() {
	JFrame myframe = new JFrame();
	 myframe.setSize(640,640);
	 myframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	 myframe.setLocationRelativeTo(null);
	 myframe.setResizable(false);
	 myframe.setVisible(true);
	return myframe;

}



public static int calcularLimiar (int atual, int limiar) {
	if (atual <= limiar)
		atual = 0;
	else if(atual > limiar)
		atual = 255;
	return atual;
}
//IMAGEM BINARIZADA
public static BufferedImage imagemBinarizada (BufferedImage image, int limiar) {
	 int altura = image.getHeight();
	 int largura = image.getWidth();
	 BufferedImage imagemsaidaBin = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);

	 for (int lin = 0; lin < altura; lin++) {
		 	for (int col = 0; col < largura; col++) {
		 		int rgb = image.getRGB(col, lin);
		 		Color cor = new Color(rgb);
		 		int red = cor.getRed();
		 		int green = cor.getGreen();
		 		int blue = cor.getBlue();
		 		int media = (red+green+blue)/3;
		 		int corLimiar = calcularLimiar(media, limiar);
		 		Color corBinaria = new Color(corLimiar, corLimiar, corLimiar);
		 		imagemsaidaBin.setRGB(col, lin, corBinaria.getRGB());
			 }
	}

	 return imagemsaidaBin;
}

//mostrar banda vermelha
public static BufferedImage bandaR (BufferedImage image) {
	 int altura = image.getHeight();
	 int largura = image.getWidth();
	 BufferedImage imagemsaidaR = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
	 for (int lin = 0; lin < altura; lin++) {
		 	for (int col = 0; col < largura; col++) {
		 		int rgb = image.getRGB(col, lin);
		 		Color cor = new Color(rgb);
		 		int red = cor.getRed();
		 		Color vermelhoApenas = new Color(red, 0, 0);
		 		imagemsaidaR.setRGB(col, lin, vermelhoApenas.getRGB());
			 }
	}

	 return imagemsaidaR;
}

public static BufferedImage bandaB (BufferedImage image) {
	 int altura = image.getHeight();
	 int largura = image.getWidth();
	 BufferedImage imagemsaidaB = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);

	 for (int lin = 0; lin < altura; lin++) {
		 	for (int col = 0; col < largura; col++) {
		 		int rgb = image.getRGB(col, lin);
		 		Color cor = new Color(rgb);
		 		int azul = cor.getBlue();
		 		Color azulApenas = new Color(0, 0, azul);
		 		imagemsaidaB.setRGB(col, lin, azulApenas.getRGB());
			 }
	}

	 return imagemsaidaB;
}

public static BufferedImage bandaG (BufferedImage image) {
	 int altura = image.getHeight();
	 int largura = image.getWidth();
	 BufferedImage imagemsaidaG = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);

	 for (int lin = 0; lin < altura; lin++) {
		 	for (int col = 0; col < largura; col++) {
		 		int rgb = image.getRGB(col, lin);
		 		Color cor = new Color(rgb);
		 		int verde = cor.getBlue();
		 		Color azulApenas = new Color(0, verde, 0);
		 		imagemsaidaG.setRGB(col, lin, azulApenas.getRGB());
			 }
	}

	 return imagemsaidaG;
  }
}